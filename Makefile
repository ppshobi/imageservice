export pwd = $(shell pwd)
CONTAINER_ID := $(shell docker container ls  | grep 'imageservice' | awk '{print $$1}')

.PHONY: start
start:
	docker run -d -p 9008:80 -v ${pwd}/php/php.ini:/usr/local/etc/php/php.ini:ro -v ${pwd}:/app imageservice

.PHONY: build
build:
	docker build -t imageservice:latest .

.PHONY: test
test:
ifdef CONTAINER_ID
	docker exec -it ${CONTAINER_ID} './vendor/bin/phpunit'
else
	@echo "Start container first with `make start`"
endif
