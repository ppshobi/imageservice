# Image Service

I have built a simple regex router and template render utility for this project. which you can see in `src/Router.php` and `src/PhpTemplateRenderer.php`.
The entry point of the app is in `public/index.php`

## Installation and running
The basic bootstrap scripts are written in a `Makefile` under the root directory. hopefully, you have `make` in your system, or you can copy-paste the commands from the makefile into the terminal

1. make sure you have docker installed
2. run `$ make build` - this will build the docker image and tag it as `imageservice:latest` (PHP is compiled with jpeg, png, and webp support)
3. run `$ make start` - this start the container in detached mode and expose a port `9008` to the host. You could change it in the Makefile
4. You should be able to access the app in the url `http://localhost:9008`
5. click on any link and you should see the page with image and its modification instructions.
6. You can modify the image by playing with the query parameters eg: `http://localhost:9008/c.webp?cropWidth=500&cropHeight=300&scaleWidth=200&scaleHeight=150`
7. run `$ make test` - to run the tests, there are some simple integration & unit tests. it will be run inside the container.

### Screenshots
***
**Homepage**
![Homepage](screenshots/homepage.png)
**Image Unmodified**
![Homepage](screenshots/image-unmodified.png)
**Image Modified**
![Homepage](screenshots/image-modified.png)
## Code Structure
The entry point of the app is in `public/index.php`

There are two controllers
1. `src/Controller/HomeController.php` - lists the existing images from `public/images` directory as a simple list
2. `src/Controller/ImageController.php` - shows the original and modified(cropped and/or scaled) images side by side

templates are in `/templates` directory

The actual service that does the image cropping/scaling is inside `src/Services` directory.

The tests are under `/tests`

In the end I ran PHPstan with max level, and linted with cs fixer. But PHP stan requires a bit more work, but mostly it is happy.
