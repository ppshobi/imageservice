<?php

namespace Tests\Unit\Services;

use PHPUnit\Framework\TestCase;
use Ppshobi\Image\Services\PhpGdImageService;

class PhpGdImageServiceTest extends TestCase
{
    public function testItCanCropImage(): void
    {
        $service = new PhpGdImageService();
        $mockImage = $this->createTempImage();
        $cropWidth = 100;
        $cropHeight = 150;

        $service->crop($mockImage, $mockImage, $cropWidth, $cropHeight);

        $croppedImageInfo =getimagesize($mockImage);

        $this->assertEquals($cropWidth, $croppedImageInfo[0]);
        $this->assertEquals($cropHeight, $croppedImageInfo[1]);
        $this->assertEquals('image/jpeg', $croppedImageInfo['mime']);
    }

    public function testItCanScaleImage(): void
    {
        $service = new PhpGdImageService();
        $mockImage = $this->createTempImage();
        $scaleWidth = 100;
        $scaleHeight = 150;

        $service->scale($mockImage, $mockImage, $scaleWidth, $scaleHeight);

        $scaledImageInfo = getimagesize($mockImage);

        $this->assertEquals($scaleWidth, $scaledImageInfo[0]);
        $this->assertEquals($scaleHeight, $scaledImageInfo[1]);
        $this->assertEquals('image/jpeg', $scaledImageInfo['mime']);
    }

    public function testItThrowsExceptionOnUnsupportedImageFormat()
    {
        $width=500;
        $height=700;
        $filepath = '/app/var/myimg.gif';
        $img = imagecreatetruecolor($width, $height);
        $bg = imagecolorallocate($img, 255, 255, 255);
        imagefilledrectangle($img, 0, 0, 120, 20, $bg);
        imagegif($img, $filepath);

        $service = new PhpGdImageService();

        $scaleWidth = 100;
        $scaleHeight = 150;

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage("Unsupported image extension given gif");
        $service->scale($filepath, $filepath, $scaleWidth, $scaleHeight);

    }
    private function createTempImage($width=500, $height=700)
    {
        $filepath = '/app/var/myimg.jpg';
        $img = imagecreatetruecolor($width, $height);
        $bg = imagecolorallocate($img, 255, 255, 255);
        imagefilledrectangle($img, 0, 0, 120, 20, $bg);
        imagejpeg($img, $filepath, 100);
        return $filepath;
    }
}
