<?php

namespace Tests\Integration\Controller;

use GuzzleHttp\Client;
use Nyholm\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Ppshobi\Image\Services\ImageService;

class ImageControllerTest extends TestCase
{
    public function testItShowsOriginalImagesAndMessageByDefault(): void
    {
        $imageName = 'a.jpeg';
        $serverRequest = new ServerRequest('GET', 'http://localhost/'.$imageName);
        $client = new Client();
        $res = $client->send($serverRequest);

        $this->assertEquals(200, $res->getStatusCode());

        $responseBody = $res->getBody()->getContents();

        $this->assertStringContainsString('/images/' . $imageName, $responseBody);
        $this->assertStringContainsString('/images/' . $imageName, $responseBody);
        $this->assertStringContainsString('image was not modified.', $responseBody);
    }

    public function testItCanCropImage(): void
    {
        $imageName = 'b.png';

        $serverRequest = new ServerRequest('GET', 'http://localhost/' . $imageName . "?cropWidth=500&cropHeight=500");
        $client = new Client();
        $res = $client->send($serverRequest);

        $this->assertEquals(200, $res->getStatusCode());

        $responseBody = $res->getBody()->getContents();

        $this->assertStringContainsString('/images/'.$imageName, $responseBody);
        $this->assertStringContainsString('/images/modified/' .$imageName, $responseBody);
    }

    public function testItCanScaleImage(): void
    {
        $imageName = 'c.webp';

        $serverRequest = new ServerRequest('GET', 'http://localhost/' . $imageName . "?scaleWidth=500&scaleHeight=500");
        $client = new Client();
        $res = $client->send($serverRequest);

        $this->assertEquals(200, $res->getStatusCode());

        $responseBody = $res->getBody()->getContents();

        $this->assertStringContainsString('/images/'.$imageName, $responseBody);
        $this->assertStringContainsString('/images/modified/' .$imageName, $responseBody);
    }
}
