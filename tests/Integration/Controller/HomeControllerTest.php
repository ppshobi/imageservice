<?php

namespace Tests\Integration\Controller;

use GuzzleHttp\Client;
use Nyholm\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Ppshobi\Image\Services\ImageService;

class HomeControllerTest extends TestCase
{
    public function testItListsAllowedImagesFromImagesDirectory(): void
    {
        $serverRequest = new ServerRequest('GET', 'http://localhost/');
        $client = new Client();
        $res = $client->send($serverRequest);

        $this->assertEquals(200, $res->getStatusCode());

        $responseBody = $res->getBody()->getContents();

        $images = $this->getFilesFromImagesDirectory();
        foreach ($images as $image) {
            $this->assertStringContainsString($image, $responseBody);
        }
    }

    private function getFilesFromImagesDirectory(): array
    {
        $files = glob(APP_ROOT . '/images/*.*');

        $images = [];
        foreach ($files as $image) {
            if (in_array(pathinfo($image)['extension'], ImageService::ACCEPTED_IMAGE_TYPES)) {
                $images[] = basename($image);
            }
        }
        return $images;
    }
}
