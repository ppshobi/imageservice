<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
if(!$isModified) {
    echo '<div style="display:flex"><p style="margin:0 auto; color: #d73e3e;font-size:x-large;">The image was not modified. You can try to crop with <code>?cropWidth=500&cropHeight=300</code> OR or AND  <code>&scaleWidth=500&scaleHeight=300</code>appending to the url.</p></div>';
}
?>
<div style="display: flex; justify-content: space-evenly">

    <div>
        <h2>Original</h2>
        <img src="<?php echo $image['original'] ?>"/>
    </div>
    <div>
        <h2>Modified</h2>
        <img src="<?php echo $image['modified'] ?>"/>
    </div>
</div>

</body>
</html>
