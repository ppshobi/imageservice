<?php

require __DIR__ . '/../vendor/autoload.php';

use Ppshobi\Image\Router;

define('APP_ROOT', dirname(__FILE__));

/** @var string $path */
$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
if (preg_match('/\/(images)\/.*/', $path)) {
    return false; // hack to let php web server to handle the /images url to serve static assets
}

$templateRenderer = new \Ppshobi\Image\PhpTemplateRenderer();
$responseFactory = new \Nyholm\Psr7\Factory\Psr17Factory();

$router = new Router();

$homeController = new \Ppshobi\Image\Controller\HomeController($templateRenderer, $responseFactory);
$router->add('/^\/$/', [$homeController, 'index']);

$imageService = new \Ppshobi\Image\Services\PhpGdImageService();

$imageController = new \Ppshobi\Image\Controller\ImageController($templateRenderer, $responseFactory, $imageService);
$router->add('/^\/(.*?)\.('. implode('|', \Ppshobi\Image\Services\ImageService::ACCEPTED_IMAGE_TYPES).')$/', [$imageController, 'modify']);

$imageService = new \Ppshobi\Image\Services\PhpGdImageService();

$router->run();
