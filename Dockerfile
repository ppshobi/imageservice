FROM php:8.1.12-fpm-alpine

# upgrade system
RUN apk upgrade --update

# Install PHP
ENV PHP_MEMORY_LIMIT=-1
RUN apk add --no-cache \
        # zip
        libzip \
        libpng \
        libwebp \
        libjpeg \
        libjpeg-turbo  \
        freetype-dev  \
        libpng-dev  \
        libjpeg-turbo-dev \
        zlib-dev  \
        libzip-dev \
        libwebp-dev \
    && docker-php-ext-configure gd \
    --enable-gd  \
    --with-freetype  \
    --with-jpeg  \
    --with-webp

RUN docker-php-ext-install -j"$(getconf _NPROCESSORS_ONLN)" \
        zip \
        gd

# install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV COMPOSER_HTACCESS_PROTECT=0

WORKDIR /app

COPY ./ /app

RUN composer install -o --prefer-dist --no-scripts --no-interaction --no-progress --no-ansi \
    && rm -Rf ./composer.*

EXPOSE 80

CMD ["php", "-S", "0.0.0.0:80", "-t", "./public", "./public/index.php"]
