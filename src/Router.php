<?php

declare(strict_types=1);

namespace Ppshobi\Image;

use Psr\Http\Message\ResponseInterface;

class Router
{
    /** @var array<string, callable> */
    private array $routes;

    public function add(string $urlRegex, callable $methodName): void
    {
        $this->routes[$urlRegex] = $methodName;
    }

    public function run(): void
    {
        $psr17Factory = new \Nyholm\Psr7\Factory\Psr17Factory();

        $route = $this->getRouteCallable();

        if (!is_callable($route)) {
            $response = $psr17Factory->createResponse(404);

            (new \Laminas\HttpHandlerRunner\Emitter\SapiEmitter())
                ->emit($response);
            exit();
        }

        $creator = new \Nyholm\Psr7Server\ServerRequestCreator(
            $psr17Factory, // ServerRequestFactory
            $psr17Factory, // UriFactory
            $psr17Factory, // UploadedFileFactory
            $psr17Factory  // StreamFactory
        );

        $serverRequest = $creator->fromGlobals();

        /** @var ResponseInterface $response */
        $response = call_user_func($route, $serverRequest);

        (new \Laminas\HttpHandlerRunner\Emitter\SapiEmitter())
            ->emit($response);
    }


    private function getRouteCallable(): ?callable
    {
        /** @var string $requestUrl */
        $requestUrl = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

        foreach ($this->routes as $urlRegex => $routeCallable) {
            $match = preg_match($urlRegex, $requestUrl);
            if ($match) {
                return $routeCallable;
            }
        }

        return null;
    }
}
