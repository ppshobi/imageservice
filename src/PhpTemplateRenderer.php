<?php

namespace Ppshobi\Image;

class PhpTemplateRenderer implements TemplateRenderer
{
    /**
     * @param array<mixed> $params
     */
    public function render(string $templateName, array $params): string
    {
        ob_start();
        extract($params, EXTR_SKIP);
        include(APP_ROOT . '/../templates/'.$templateName);
        /** @var string $output */
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}
