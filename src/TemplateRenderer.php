<?php

namespace Ppshobi\Image;

interface TemplateRenderer
{
    /**
     * @param array<mixed> $params
     */
    public function render(string $templateName, array $params): string;
}
