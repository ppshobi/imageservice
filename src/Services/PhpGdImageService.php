<?php

namespace Ppshobi\Image\Services;

use RuntimeException;

class PhpGdImageService implements ImageService
{
    public function crop(string $imageSourcePath, string $imageDestinationPath, int $cropWidth, int $cropHeight, int $cropStartX=0, int $cropStartY=0): string
    {
        list($extension, $image) = $this->createGdImageFromPath($imageSourcePath);

        /** @var \GdImage $cropped */
        $cropped = imagecrop($image, [
            'x' => $cropStartX,
            'y' => $cropStartY,
            'width' => $cropWidth,
            'height'=> $cropHeight
        ]);

        $this->convertToExpectedExtension($extension, $cropped, $imageDestinationPath);

        return $imageDestinationPath;
    }

    private function getConverterFunction(string $extension): string
    {
        try {
            return match ($extension) {
                'jpg' => 'imagejpeg',
                'jpeg' => 'imagejpeg',
                'png' => 'imagepng',
                'webp' => 'imagewebp'
            };
        } catch (\UnhandledMatchError $e) {
            throw new \RuntimeException("Unsupported image extension given " . $extension);
        }
    }

    public function scale(string $imageSourcePath, string $imageDestinationPath, int $scaleWidth, int $scaleHeight): string
    {
        list($extension, $image) = $this->createGdImageFromPath($imageSourcePath);

        /** @var \GdImage $scaled */
        $scaled = imagescale($image, $scaleWidth, $scaleHeight);

        $this->convertToExpectedExtension($extension, $scaled, $imageDestinationPath);

        return $imageDestinationPath;
    }

    private function getExtension(string $imageSourcePath): string
    {
        $imageInfo = getimagesize($imageSourcePath);
        if (!$imageInfo) {
            throw new RuntimeException("Unable to detect image mime for the given image". $imageSourcePath);
        }
        return explode("/", $imageInfo['mime'])[1];
    }


    private function createGdImageFromPath(string $imageSourcePath): array
    {
        $extension = $this->getExtension($imageSourcePath);

        /** @var string $imageContent */
        $imageContent = file_get_contents($imageSourcePath);

        /** @var \GdImage $image */
        $image = imagecreatefromstring($imageContent);

        return array($extension, $image);
    }

    private function convertToExpectedExtension(string $extension, \GdImage $gdImage, string $imageDestinationPath): void
    {
        /** @var callable $converterFunction */
        $converterFunction = $this->getConverterFunction($extension);
        $converterFunction($gdImage, $imageDestinationPath);
    }
}
