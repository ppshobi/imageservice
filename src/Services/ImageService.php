<?php

namespace Ppshobi\Image\Services;

interface ImageService
{
    public const ACCEPTED_IMAGE_TYPES = ['jpeg', 'jpg', 'png', 'webp'];
    public function crop(string $imageSourcePath, string $imageDestinationPath, int $cropWidth, int $cropHeight, int $cropStartX=0, int $cropStartY=0): string;
    public function scale(string $imageSourcePath, string $imageDestinationPath, int $scaleWidth, int $scaleHeight): string;
}
