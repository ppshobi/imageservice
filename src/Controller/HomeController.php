<?php

namespace Ppshobi\Image\Controller;

use Ppshobi\Image\Services\ImageService;
use Ppshobi\Image\TemplateRenderer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class HomeController extends AbstractController
{
    public function index(ServerRequestInterface $request): ResponseInterface
    {
        $images = $this->getValidImagesList();

        $response = $this->renderer->render('home.template.php', ['images' => $images]);

        return $this->sendResponse($response);
    }

    /** @return array<string> */
    private function getValidImagesList(): array
    {
        $dirName = APP_ROOT . '/images';
        /** @var array<string> $files */
        $files = glob($dirName . '/*.*');

        $images = [];
        foreach ($files as $image) {
            if (in_array(pathinfo($image)['extension'] ?? null, ImageService::ACCEPTED_IMAGE_TYPES)) {
                $images[] = basename($image);
            }
        }
        return $images;
    }
}
