<?php

namespace Ppshobi\Image\Controller;

use Nyholm\Psr7\Response;
use Nyholm\Psr7\ServerRequest;
use Ppshobi\Image\Services\PhpGdImageService;
use Ppshobi\Image\TemplateRenderer;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ImageController extends AbstractController
{
    private PhpGdImageService $imageService;

    public function __construct(TemplateRenderer $renderer, ResponseFactoryInterface $responseFactory, PhpGdImageService $imageService)
    {
        parent::__construct($renderer, $responseFactory);
        $this->imageService = $imageService;
    }

    public function modify(ServerRequestInterface $request): ResponseInterface
    {
        $queryParams = $request->getQueryParams();

        $imageName = ltrim($request->getUri()->getPath(), '/');
        $assetDirectory = APP_ROOT.'/images/';

        $tmpFilePath = $this->createTemporaryFile($assetDirectory . $imageName);


        $hasCropped = $this->cropImage($queryParams, $tmpFilePath);
        $hasScaled = $this->scaleImage($queryParams, $tmpFilePath);

        if ($hasCropped || $hasScaled) {
            $destinationPath = '/modified/' . $imageName;
            copy($tmpFilePath, $assetDirectory . $destinationPath);
            unlink($tmpFilePath);
            $modifiedImagePath = '/images/modified/' . $imageName;
        }

        $originalImagePath = '/images/'.$imageName;

        $response = $this->renderer->render('image.template.php', [
            'image' => [
                'original' => $originalImagePath,
                'modified' => $modifiedImagePath ?? $originalImagePath,
            ],
            'isModified' => $hasCropped || $hasScaled,
        ]);


        return $this->sendResponse($response);
    }

    private function createTemporaryFile(string $filePath): string
    {
        /** @var string $tmpfile */
        $tmpfile = tempnam(sys_get_temp_dir(), 'tmpimg');
        file_put_contents($tmpfile, file_get_contents($filePath));

        return $tmpfile;
    }


    /**
     * @param array<string,int> $queryParams
     */
    private function cropImage(array $queryParams, string $tmpFilePath): bool
    {
        $isModified = false;
        if (isset($queryParams['cropWidth']) && isset($queryParams['cropHeight'])) {
            $isModified = true;
            $this->imageService->crop(
                $tmpFilePath,
                $tmpFilePath,
                $queryParams['cropWidth'],
                $queryParams['cropHeight'],
            );
        }
        return $isModified;
    }

    /**
     * @param array<string,int> $queryParams
     */
    private function scaleImage(array $queryParams, string $tmpFilePath): bool
    {
        $isModified = false;
        if (isset($queryParams['scaleWidth']) && isset($queryParams['scaleHeight'])) {
            $isModified = true;
            $this->imageService->scale(
                $tmpFilePath,
                $tmpFilePath,
                $queryParams['scaleWidth'],
                $queryParams['scaleHeight'],
            );
        }
        return $isModified;
    }
}
