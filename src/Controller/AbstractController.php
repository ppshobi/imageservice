<?php

namespace Ppshobi\Image\Controller;

use Ppshobi\Image\TemplateRenderer;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;

class AbstractController
{
    protected TemplateRenderer $renderer;
    private ResponseFactoryInterface $responseFactory;

    public function __construct(TemplateRenderer $renderer, ResponseFactoryInterface $responseFactory)
    {
        $this->renderer = $renderer;
        $this->responseFactory = $responseFactory;
    }

    public function sendResponse(string $response): ResponseInterface
    {
        $responseBody = $this->responseFactory->createStream($response);

        return $this->responseFactory->createResponse(200)->withBody($responseBody);
    }
}
